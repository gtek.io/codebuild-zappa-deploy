#!/bin/bash

#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#


. build-docker-image.sh

echo "Need to fix 'if checks' for filesystem artifacts."
exit -99

if [[ ! -r ../../edas-ps-api ]]
then
	echo "This script should be run from devops/buildapi"
    exit 1
fi

#echo $(for l in "${apienv}";do echo ${l} | cut -d" " -f 2 | echo;done)


docker run --rm -it \
    -e env -v ${workdir}:/work \
    --env-file docker-env.tmp \
    -v $(realpath ../../${repo_name}):/var/task \
    "gtek/${repo_name}" \
    -c "pipenv install; PYTHONPATH=. pipenv run bash"
    #-c "cd /work;. ${envfile}; clear; bash"


rm docker-env.tmp

