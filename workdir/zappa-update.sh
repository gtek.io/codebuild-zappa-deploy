#!/bin/bash

#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#


echo ""
echo "Running $0 inside the zappa-lambda docker container."
echo ""

if [[ -z "$1" ]]
then
	echo "You must pass an environment name as a command line argument to this program"
	echo "Ex: 'dev' or 'prod'"
	exit 1
fi


cd /work
repodir="/repo"

if [[ ! -d "${repodir}" ]]
then

	echo "Invalid repo directory: ${repodir}"

fi


echo "Deploying API to dev environment"

# . /work/api-settings.dev.sh

settings_file="zappa_settings.json"

cp -a /work/${settings_file} ${repodir}

zappa update $1

rm ${repodir}/${settings_file}


# Junk in the basement
####################################################



# if [[ "$1" == "dev" ]]
# then
# 	echo "Deploying API to dev environment"

# 	. /work/api-settings.dev.sh

# 	settings_file="zappa_settings.json"

# 	cp -a /work/${settings_file} ${repodir}
	
# 	cd ${repodir}
	
# 	pipenv sync
# 	pipenv install zappa
# 	. /work/api-settings.dev.sh ; pipenv run zappa update dev

# 	#rm ${settings_file}

# fi

