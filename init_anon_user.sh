#!/bin/bash

. build-docker-image.sh

. workdir/api-settings.${env}.sh

if [[ -z "${MONGODB_DRIVER_STRING}" ]] 
then
	echo "you must set the MONGODB_DRIVER_STRING env var"
	exit 1
fi

if [[ ! -r ../../edas-ps-api ]]
then
	echo "This script should be run from devops/buildapi"
    exit 1
fi


# TEST REPLACING -c stuff with $cmdrun from build-docker-image.sh
docker run --rm -it \
    -e env -v ${workdir}:/work \
    --env-file docker-env.tmp \
    -v $(realpath ../../${repo_name}):/var/task \
    "gtek/${repo_name}" \
    -c "echo '>>> -c RUNNING'; pwd;pipenv install; PYTHONPATH=. pipenv run python api/scripts/create_anon_user.py"


rm docker-env.tmp

