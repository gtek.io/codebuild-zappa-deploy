#!/bin/bash

#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#


codedir=`pwd`
name=codebuild-zappa-deploy
dir_path=$(dirname $(realpath $0))

cd /tmp

if [[ -d ${name} ]];
then
    
    rm -rf ${name}

fi

mkdir ${name}
cp -a ${dir_path}/* ${name}
rm ${name}/$(basename $(realpath ${0}))


tar --exclude-vcs -cf ${name}.tar ${name} 
gzip -f9 ${name}.tar
aws --profile gortek-iam-tony s3 mv ${name}.tar.gz s3://${name}/

rm -rf ${name}
rm -f ${name}.tar.gz



