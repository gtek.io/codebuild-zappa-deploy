#!/bin/bash

if [[ -z "${repodir}" ]]
then
    echo "You must set an environment variable named 'repodir' to use this program."
    echo "It should be the path to your code to be deployed."
    exit 1
fi

. build-docker-image.sh

# echo "\n\n"
# echo 'printing output of $(./shenv2dockerenv.py ${h_envfile} )'
# echo "$(./shenv2dockerenv.py ${h_envfile} )"
# echo "\n\nprinting done\n\n"

#-v $(realpath ../../${repo_name}):/var/task "gtek/${repo_name}" \

docker run --rm -it \
    -e env \
    --env-file docker-env.tmp \
    -v ${h_workdir}:/work \
    -v ${repodir}:/var/task \
    "gtek/${image_name}" \
    -c "cp -a /work/zappa_settings.json .; pipenv install; pipenv run zappa update ${env}; pipenv run zappa status"
    # -c "${cmdinit} ${cmdrun} /work/build.sh ${env}; ${cmdrun} bash"

rm docker-env.tmp
