
#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#

from lambci/lambda:build-python3.7
#from ubuntu:rolling
#from python:3.7-alpine

WORKDIR /var/task

RUN echo 'export PATH="/repo/.venv/bin:$PATH"' >> /root/.bashrc
RUN echo 'export PS1="\[\e[36m\]zappashell>\[\e[m\] "' >> /root/.bashrc


#alpine#RUN apk --update add --no-cache zlib zlib-dev
#ubuntu#RUN apt update && apt upgrade -y #build-essential
#RUN apt install -y python3 python3-pip

# Amazon Linux (centos)
RUN pip3 install pipenv


ENTRYPOINT ["bash"]
#CMD ["bash"]
