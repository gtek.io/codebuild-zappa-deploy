# CodeBuild Zappa Deploy

Scripts and Python config to enable Zappa deployment from within AWS CodeBuild.


From within your buildspec.yml file, add these pre-build steps:

- curl -o /tmp/deployer.tar.gz https://codebuild-zappa-deploy.s3-us-west-2.amazonaws.com/codebuild-zappa-deploy.tar.gz
- tar -xfz -C /tmp  /tmp/deployer.tar.gz

You will then have a /tmp/codebuild-zappa-deploy directory that contains the zappa docker image for deploying your app.

-  

```
#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#
```
