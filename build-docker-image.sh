#!/bin/bash

#
# Copyright 2020 Gortek, LLC. All rights reserved.
#
# Use, distribution, and sale of this software is 
# prohibited except for parties granted explicit 
# written license to do so.
#


if [[ -z "$1" ]]
then
	echo "You must pass an environment name as a command line argument to this program"
	echo "Either 'dev' or 'prod'"
	exit 1
fi

# if [[ -z "${image_name}" ]]
# then
#     echo "An environment named 'image_name' (used for the docker image name) is required"
#     exit 1
# fi

export env=${1}

# ENV vars used in the docker command 
export dir_path=$(dirname $(realpath $0))
export h_workdir="${dir_path}/workdir"
export c_workdir=/var/task
export image_name="codebuild-zappa-deploy"

# specialized path in the host environment
h_envfile=${h_workdir}/api-settings.${env}.sh
# specialized path inside the container
#c_envfile="/work/api-settings.${env}.sh"

# . ${h_envfile}

export existing_image=$(docker ps -a | grep "gtek/${image_name}")

if [[ -z "${existing_image}" ]]
then
	echo "Building docker image - name: gtek/${image_name}"
	docker build -t gtek/${image_name} . 
fi


# cmdinit='. '"${c_envfile}; cd /repo; pipenv install;"
# cmdrun="cd /repo; PYTHONPATH=/repo pipenv run"

./shenv2dockerenv.py ${h_envfile} > docker-env.tmp

echo ""
echo ""
echo ""

cat docker-env.tmp

echo ""
echo ""
echo ""

echo "CHECKPOINT: docker-image-initialized; image-name: ${image_name}"
